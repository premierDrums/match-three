# find treasure
height_rider = int(input("how much height have rider? "))

if height_rider < 50:
    print("you can't ride the horse")
else:
    print("It's okay to ride")

age = int(input("How old are you? "))

if age > 50:
    print("You are too old to ride")
    exit()  # Program stops here if age > 50

# Continue if the rider is not too old
if age > 19:
    bill = 17
elif age <= 19:
    bill = 10

photo = input("Do you want a photo? ")

if photo == "yes":
    bill += 3  # Add 3 to the bill if they want a photo
    print(f"Your price is {bill}")
elif photo == "no":
    print(f"Your price is {bill}")
